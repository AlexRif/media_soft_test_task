insert into mediasoft_test_task.MAILING (id, type, addressee_Index, addressee_Address, ADDRESSEE_FULL_NAME)
VALUES ('a7d43180-f014-42dc-9e96-aa30d89ccffe', 'litter', '443067', 'г. Самара, ул. Советской Армии, д. 113',
        'Странный Игорь Александрович');
insert into mediasoft_test_task.MAILING (id, type, addressee_Index, addressee_Address, ADDRESSEE_FULL_NAME)
VALUES ('46ad9979-0f67-4df3-b850-b17c3b912b56', 'litter', '446370',
        'обл. Самарская, р-н Красноярский, с. Красный Яр, д. 12',
        'Занимательный Игорь Семёнович');
insert into mediasoft_test_task.POST_OFFICE (id, INDEX, NAME)
values ('0bad353c-1557-4c98-b9f9-74af4bdc0445', '443066', 'Почтовое отделение Самара 66');
insert into mediasoft_test_task.POST_OFFICE (id, INDEX, NAME)
values ('760d134a-4bde-44de-a725-2fac52095b46', '420300', 'ЛПЦ Тестовый');
insert into mediasoft_test_task.MAILING_MOVEMENT_LOG(ID, TYPE, MAILING_ID, POST_OFFICE_ID, CREATE_AT)
values ('b1435dfa-cc01-4706-80cc-b27dfb8c2ac7', 'registration', 'a7d43180-f014-42dc-9e96-aa30d89ccffe',
        null, '2022-11-09 12:45:06');
insert into mediasoft_test_task.MAILING_MOVEMENT_LOG(ID, TYPE, MAILING_ID, POST_OFFICE_ID, CREATE_AT)
values ('c857190c-833e-4bbf-b7c6-29b6fd1f271d', 'arrival', 'a7d43180-f014-42dc-9e96-aa30d89ccffe',
        '760d134a-4bde-44de-a725-2fac52095b46', '2022-11-10 12:45:06');
insert into mediasoft_test_task.MAILING_MOVEMENT_LOG(ID, TYPE, MAILING_ID, POST_OFFICE_ID, CREATE_AT)
values ('4ae10d1c-4e96-47ca-865b-d2988c6d38a2', 'departure', 'a7d43180-f014-42dc-9e96-aa30d89ccffe',
        '760d134a-4bde-44de-a725-2fac52095b46', '2022-11-11 12:45:06');
insert into mediasoft_test_task.MAILING_MOVEMENT_LOG(ID, TYPE, MAILING_ID, POST_OFFICE_ID, CREATE_AT)
values ('252f7325-ab39-4a27-803f-f7e6dbf65d65', 'arrival', 'a7d43180-f014-42dc-9e96-aa30d89ccffe',
        '0bad353c-1557-4c98-b9f9-74af4bdc0445', '2022-11-12 12:45:06');
insert into mediasoft_test_task.MAILING_MOVEMENT_LOG(ID, TYPE, MAILING_ID, POST_OFFICE_ID, CREATE_AT)
values ('8ef3daa2-022f-4975-81ba-71b97811ec91', 'departure', 'a7d43180-f014-42dc-9e96-aa30d89ccffe',
        '0bad353c-1557-4c98-b9f9-74af4bdc0445', '2022-11-13 12:45:06');
insert into mediasoft_test_task.MAILING_MOVEMENT_LOG(ID, TYPE, MAILING_ID, POST_OFFICE_ID, CREATE_AT)
values ('f1c24874-6203-4978-8b06-ec2e9b2b2429', 'delivery', 'a7d43180-f014-42dc-9e96-aa30d89ccffe',
        null, '2022-11-14 12:45:06');


