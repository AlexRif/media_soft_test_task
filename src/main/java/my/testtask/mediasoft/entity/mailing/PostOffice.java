package my.testtask.mediasoft.entity.mailing;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Table(name = "POST_OFFICE")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostOffice {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "INDEX")
    private String index;

    @Column(name = "NAME")
    private String address;
}
