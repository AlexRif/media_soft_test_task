package my.testtask.mediasoft.entity.mailing.enums;

import jakarta.annotation.Nullable;

public enum MailingType {
    LITTER("litter"),
    DISPATCH("dispatch"),
    WRAPPER("wrapper"),
    POSTCARD("postcard");

    private String id;

    MailingType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static MailingType fromId(String id) {
        for (MailingType at : MailingType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
