package my.testtask.mediasoft.entity.mailing.enums;

import jakarta.annotation.Nullable;

public enum MailingMovementType {

    REGISTRATION("registration"),
    ARRIVAL("arrival"),
    DEPARTURE("departure"),
    DELIVERY("delivery");

    private String id;

    MailingMovementType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static MailingMovementType fromId(String id) {
        for (MailingMovementType at : MailingMovementType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
