package my.testtask.mediasoft.entity.mailing;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.testtask.mediasoft.entity.mailing.enums.MailingType;

import java.util.UUID;

@Entity
@Table(name = "MAILING")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Mailing {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "TYPE", nullable = false)
    private String type;

    @Column(name = "ADDRESSEE_INDEX", nullable = false)
    private String addresseeIndex;

    @Column(name = "ADDRESSEE_ADDRESS", nullable = false)
    private String addresseeAddress;

    @Column(name = "ADDRESSEE_FULL_NAME", nullable = false)
    private String fullName;

    public MailingType getType() {
        return this.type != null ? MailingType.fromId(type) : null;
    }

    public void setType(MailingType type) {
        this.type = type != null ? type.getId() : null;
    }
}
