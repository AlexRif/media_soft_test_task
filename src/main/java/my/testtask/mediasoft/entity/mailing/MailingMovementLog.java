package my.testtask.mediasoft.entity.mailing;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.testtask.mediasoft.entity.mailing.enums.MailingMovementType;

import java.util.Date;
import java.util.UUID;


@Entity(name = "MAILING_MOVEMENT_LOG")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailingMovementLog {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "TYPE", nullable = false)
    private String type;

    @ManyToOne
    @JoinColumn(name = "MAILING_ID", nullable = false)
    private Mailing mailing;

    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_AT")
    private Date createAt;

    @ManyToOne
    @JoinColumn(name = "POST_OFFICE_ID")
    private PostOffice postOffice;

    public MailingMovementLog(MailingMovementType type, Mailing mailing, PostOffice postOffice) {
        setType(type);
        setMailing(mailing);
        setCreateAt(new Date());
        setPostOffice(postOffice);
    }

    public MailingMovementType getType() {
        return this.type != null ? MailingMovementType.fromId(type) : null;
    }

    public void setType(MailingMovementType type) {
        this.type = type != null ? type.getId() : null;
    }
}
