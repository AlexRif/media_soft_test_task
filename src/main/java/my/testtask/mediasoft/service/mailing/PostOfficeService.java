package my.testtask.mediasoft.service.mailing;

import my.testtask.mediasoft.dto.PostOfficeBasicRequest;
import my.testtask.mediasoft.entity.mailing.PostOffice;

public interface PostOfficeService {

    PostOffice init(PostOfficeBasicRequest request);
}
