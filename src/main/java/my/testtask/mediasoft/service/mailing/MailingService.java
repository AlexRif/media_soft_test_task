package my.testtask.mediasoft.service.mailing;


import my.testtask.mediasoft.dto.MailingRequest;
import my.testtask.mediasoft.entity.mailing.Mailing;

public interface MailingService {

    Mailing registry(MailingRequest mailingRequest);

}
