package my.testtask.mediasoft.service.mailing;

import my.testtask.mediasoft.dto.MailingMovementLogRequest;
import my.testtask.mediasoft.entity.mailing.Mailing;
import my.testtask.mediasoft.entity.mailing.MailingMovementLog;
import my.testtask.mediasoft.entity.mailing.PostOffice;
import my.testtask.mediasoft.entity.mailing.enums.MailingMovementType;

import javax.validation.constraints.NotNull;

public interface MailingMovementLogService {

    MailingMovementLog createAndSave(MailingMovementType type, Mailing mailing, PostOffice postOffice);

    MailingMovementLog registry(MailingMovementLogRequest request);

    MailingMovementLog registryDeparture(@NotNull Mailing mailing, @NotNull PostOffice postOffice);

    boolean isAlreadyRegistered(MailingMovementType type,@NotNull  Mailing mailing,@NotNull  PostOffice postOffice);
}
