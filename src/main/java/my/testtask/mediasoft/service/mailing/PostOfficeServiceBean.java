package my.testtask.mediasoft.service.mailing;

import my.testtask.mediasoft.dto.PostOfficeBasicRequest;
import my.testtask.mediasoft.entity.mailing.PostOffice;
import my.testtask.mediasoft.exception.ProcessException;
import my.testtask.mediasoft.mapper.RequestMapper;
import my.testtask.mediasoft.repository.PostOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostOfficeServiceBean implements PostOfficeService{

    @Autowired
    private RequestMapper<PostOffice, PostOfficeBasicRequest> postOfficeBasicRequestMapper;

    @Autowired
    private PostOfficeRepository postOfficeRepository;

    @Override
    public PostOffice init(PostOfficeBasicRequest request) {
        if (!isValidRequest(request)) {
            throw new ProcessException("Fill in all required fields");
        }
        PostOffice postOffice = new PostOffice();
        postOffice = postOfficeBasicRequestMapper.toEntity(request, postOffice);
        return postOfficeRepository.save(postOffice);
    }

    private boolean isValidRequest(PostOfficeBasicRequest request) {
        return request != null && request.getAddress() != null && request.getIndex() != null;
    }
}
