package my.testtask.mediasoft.service.mailing;

import my.testtask.mediasoft.dto.MailingMovementLogRequest;
import my.testtask.mediasoft.entity.mailing.Mailing;
import my.testtask.mediasoft.entity.mailing.MailingMovementLog;
import my.testtask.mediasoft.entity.mailing.PostOffice;
import my.testtask.mediasoft.entity.mailing.enums.MailingMovementType;
import my.testtask.mediasoft.exception.ProcessException;
import my.testtask.mediasoft.repository.MailingMovementLogRepository;
import my.testtask.mediasoft.repository.MailingRepository;
import my.testtask.mediasoft.repository.PostOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class MailingMovementLogServiceBean implements MailingMovementLogService {

    @Autowired
    private MailingMovementLogRepository mailingMovementLogRepository;
    @Autowired
    private MailingRepository mailingRepository;
    @Autowired
    private PostOfficeRepository postOfficeRepository;

    private final Set<MailingMovementType> withoutOfficeTypes =
            Set.of(MailingMovementType.REGISTRATION, MailingMovementType.DELIVERY);

    @Override
    public MailingMovementLog createAndSave(MailingMovementType type, Mailing mailing, PostOffice postOffice) {
        return mailingMovementLogRepository.save(new MailingMovementLog(type, mailing, postOffice));
    }

    /**
     *
     * @param request
     * @return
     */
    @Override
    public MailingMovementLog registry(MailingMovementLogRequest request) {
        if (!isValidRequest(request)) {
            throw new ProcessException("Fill in all required fields");
        }
        Mailing mailing = mailingRepository.findById(request.getMailingId())
                .orElseThrow(() -> new ProcessException("Mailing not found"));
        PostOffice postOffice = null;
        if (!withoutOfficeTypes.contains(request.getType())) {
            if (request.getPostOfficeId() == null) {
                throw new ProcessException("Post office ID required");
            }
            postOffice = postOfficeRepository.findById(request.getPostOfficeId())
                    .orElseThrow(() -> new ProcessException("Post office not found"));
        }
        if (isAlreadyRegistered(request.getType(), mailing, postOffice)) {
            return mailingMovementLogRepository
                    .findByTypeAndMailingAndPostOffice(request.getType().getId(), mailing, postOffice)
                    .get();
        }
        MailingMovementLog movementLog = null;
        switch (request.getType()) {
            case REGISTRATION, DELIVERY, ARRIVAL -> movementLog = createAndSave(request.getType(), mailing, postOffice);
            case DEPARTURE -> movementLog = registryDeparture(mailing, postOffice);
        }
        if (movementLog == null) {
            throw new ProcessException("Unexpected mailing registration error");
        }
        return movementLog;
    }


    @Override
    public MailingMovementLog registryDeparture(Mailing mailing, PostOffice postOffice) {
        Optional<MailingMovementLog> arrivalMovementLogOptional = mailingMovementLogRepository
                .findByTypeAndMailing(MailingMovementType.ARRIVAL.getId(), mailing);
        if (arrivalMovementLogOptional.isEmpty()) {
            throw new ProcessException("It is not possible to register the departure from the post office of a mailing" +
                    " for which the arrival was not registered");
        }
        if (isAlreadyRegistered(MailingMovementType.DEPARTURE, mailing, postOffice)) {
            return mailingMovementLogRepository
                    .findByTypeAndMailingAndPostOffice(MailingMovementType.DELIVERY.getId(), mailing, postOffice).get();
        }
        return createAndSave(MailingMovementType.DEPARTURE, mailing, postOffice);
    }

    @Override
    public boolean isAlreadyRegistered(MailingMovementType type, Mailing mailing, PostOffice postOffice) {
        return mailingMovementLogRepository
                .findByTypeAndMailingAndPostOffice(type.getId(), mailing, postOffice)
                .isPresent();
    }


    private boolean isValidRequest(MailingMovementLogRequest request) {
        return !(request == null
                || request.getMailingId() == null
                || request.getType() == null);
    }
}
