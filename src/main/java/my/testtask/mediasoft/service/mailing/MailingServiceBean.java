package my.testtask.mediasoft.service.mailing;

import lombok.RequiredArgsConstructor;
import my.testtask.mediasoft.assit.Utils;
import my.testtask.mediasoft.dto.MailingRequest;
import my.testtask.mediasoft.entity.mailing.Mailing;
import my.testtask.mediasoft.entity.mailing.MailingMovementLog;
import my.testtask.mediasoft.entity.mailing.enums.MailingMovementType;
import my.testtask.mediasoft.exception.ProcessException;
import my.testtask.mediasoft.mapper.RequestMapper;
import my.testtask.mediasoft.repository.MailingMovementLogRepository;
import my.testtask.mediasoft.repository.MailingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class MailingServiceBean implements MailingService {

    @Autowired
    private MailingRepository mailingRepository;
    @Autowired
    private MailingMovementLogRepository mailingMovementLogRepository;
    @Autowired
    private RequestMapper<Mailing, MailingRequest> mailingRequestMapper;

    @Override
    @Transactional
    public Mailing registry(MailingRequest mailingRequest) {
        if (!isValidRequest(mailingRequest)) {
            throw new ProcessException("Fill in all required fields");
        }
        Mailing mailing = new Mailing();
        mailingRequestMapper.toEntity(mailingRequest, mailing);
        mailing = mailingRepository.save(mailing);
        MailingMovementLog mailingMovementLog = new MailingMovementLog(MailingMovementType.REGISTRATION, mailing, null);
        mailingMovementLogRepository.save(mailingMovementLog);
        return mailing;
    }

    private boolean isValidRequest(MailingRequest mailingRequest) {
        return !(mailingRequest == null
                || Utils.isNullOrEmpty(mailingRequest.getAddresseeAddress())
                || Utils.isNullOrEmpty(mailingRequest.getAddresseeIndex())
                || Utils.isNullOrEmpty(mailingRequest.getFullName())
                || mailingRequest.getType() == null);
    }
}
