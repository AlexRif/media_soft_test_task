package my.testtask.mediasoft.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import my.testtask.mediasoft.dto.ErrorResponse;
import my.testtask.mediasoft.dto.MailingRequest;
import my.testtask.mediasoft.entity.mailing.Mailing;
import my.testtask.mediasoft.exception.ProcessException;
import my.testtask.mediasoft.service.mailing.MailingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mailing")
@Tag(name = "MailingController", description = "Controller responsible for work with mailing")
public class MailingController {

    @Autowired
    private MailingService mailingService;

    @Operation(
            summary = "Creating a mailing"
    )
    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(@RequestBody MailingRequest mailingRequest) {
        Mailing mailing;
        try {
            mailing = mailingService.registry(mailingRequest);
        } catch (ProcessException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
        return ResponseEntity.ok(mailing.getId());
    }
}
