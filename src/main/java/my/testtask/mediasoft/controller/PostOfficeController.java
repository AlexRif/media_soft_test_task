package my.testtask.mediasoft.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import my.testtask.mediasoft.dto.ErrorResponse;
import my.testtask.mediasoft.dto.PostOfficeBasicRequest;
import my.testtask.mediasoft.dto.PostOfficeBrowseResponse;
import my.testtask.mediasoft.entity.mailing.PostOffice;
import my.testtask.mediasoft.exception.ProcessException;
import my.testtask.mediasoft.mapper.ResponseMapper;
import my.testtask.mediasoft.repository.PostOfficeRepository;
import my.testtask.mediasoft.service.mailing.PostOfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/office")
@Tag(name = "PostOfficeController", description = "Controller responsible for work with Post office")
public class PostOfficeController {

    @Autowired
    private PostOfficeService postOfficeService;

    @Autowired
    private PostOfficeRepository postOfficeRepository;
    @Autowired
    private ResponseMapper<PostOffice, PostOfficeBrowseResponse> postOfficeBrowseMapper;

    @Operation(
            summary = "Getting a list of post offices"
    )
    @GetMapping(path = "/")
    public ResponseEntity<Object> findAll() {
        return ResponseEntity.ok(postOfficeRepository.findAll().stream().map(postOfficeBrowseMapper::toResponse).collect(Collectors.toList()));
    }

    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(PostOfficeBasicRequest request) {
        try {
            PostOffice postOffice = postOfficeService.init(request);
            return ResponseEntity.ok(postOffice.getId());
        } catch (ProcessException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
}
