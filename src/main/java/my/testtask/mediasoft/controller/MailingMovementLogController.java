package my.testtask.mediasoft.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import my.testtask.mediasoft.dto.ErrorResponse;
import my.testtask.mediasoft.dto.MailingMovementLogBrowseResponse;
import my.testtask.mediasoft.dto.MailingMovementLogRequest;
import my.testtask.mediasoft.entity.mailing.Mailing;
import my.testtask.mediasoft.entity.mailing.MailingMovementLog;
import my.testtask.mediasoft.exception.ProcessException;
import my.testtask.mediasoft.mapper.ResponseMapper;
import my.testtask.mediasoft.repository.MailingMovementLogRepository;
import my.testtask.mediasoft.repository.MailingRepository;
import my.testtask.mediasoft.service.mailing.MailingMovementLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/movement")
@Tag(name = "MailingMovementLogController", description = "The controller responsible for working with the history of mailing")
public class MailingMovementLogController {

    @Autowired
    private MailingRepository mailingRepository;
    @Autowired
    private MailingMovementLogRepository mailingMovementLogRepository;
    @Autowired
    private MailingMovementLogService mailingMovementLogService;
    @Autowired
    private ResponseMapper<MailingMovementLog, MailingMovementLogBrowseResponse> movementBrowseResponseMapper;


    @Operation(
            summary = "Creating a mailing",
            description = "Allows you to register the parcel and its movement. " +
                    "Movement types are specified in MailingMovementType class"
    )
    @PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> registry(MailingMovementLogRequest mailingMovementLogRequest) {
        try {
            MailingMovementLog movementLog = mailingMovementLogService.registry(mailingMovementLogRequest);
            return ResponseEntity.ok(movementLog.getId());
        } catch (ProcessException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }


    @Operation(
            summary = "Getting the history of movements by mailing ID",
            description = "Allows you to get an array of data about the history of the movement of a mailing by mailing ID"
    )
    @GetMapping(path = "/{mailingId}")
    public ResponseEntity<Object> findAllByMailingIdOrderByCreateAt(@PathVariable String mailingId) {
        try {
            Mailing mailing = mailingRepository.findById(UUID.fromString(mailingId))
                    .orElseThrow(() -> new ProcessException("Mailing for this id was not found"));
            List<MailingMovementLog> movementLogs = mailingMovementLogRepository.findAllByMailingOrderByCreateAt(mailing);
            return ResponseEntity.ok(movementLogs.stream()
                    .map(movementBrowseResponseMapper::toResponse)
                    .collect(Collectors.toList()));
        } catch (IllegalArgumentException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResponse(String.format("Invalid ID %s", mailingId)));
        } catch (ProcessException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage()));
        }
    }
}
