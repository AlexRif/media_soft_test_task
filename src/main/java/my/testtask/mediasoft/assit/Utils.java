package my.testtask.mediasoft.assit;

import java.util.List;

public class Utils {

   public static boolean isNullOrEmpty(Object obj) {
        if (obj == null) return true;
        return isEmpty(obj);
    }


    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj instanceof Integer) {
            return false;
        }
        if (obj instanceof String) {
            return (((String) obj).trim()).isEmpty();
        }
        if (obj instanceof List) {
            return ((List) obj).isEmpty();
        }
        return true;
    }


}
