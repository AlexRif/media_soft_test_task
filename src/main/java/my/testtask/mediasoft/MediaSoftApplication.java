package my.testtask.mediasoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediaSoftApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediaSoftApplication.class, args);
	}

}
