package my.testtask.mediasoft.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(
                title = "Test task for MediaSoft",
                description = "Test task variant 3 - Java EE", version = "1.0.0",
                contact = @Contact(
                        name = "Arefev Alexandr",
                        email = "acktarad@gmail.com",
                        url = "telegram:@AL_RIF"
                )
        )
)
public class DocConfig {
}
