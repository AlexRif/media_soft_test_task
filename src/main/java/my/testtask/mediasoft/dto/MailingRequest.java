package my.testtask.mediasoft.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.testtask.mediasoft.entity.mailing.enums.MailingType;
import my.testtask.mediasoft.mapper.Request;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Standard entity for registering a mailing")
public class MailingRequest implements Request {

    @Schema(description = "Type of mailing (indicate in capital letters)", example = "LITTER")
    private String type;
    @Schema(description = "Address of the recipient")
    private String addresseeIndex;
    @Schema(description = "Recipient address index")
    private String addresseeAddress;
    @Schema(description = "Recipient's full name")
    private String fullName;

    public MailingType getType() {
        return this.type != null ? MailingType.fromId(type) : null;
    }

    public void setType(MailingType type) {
        this.type = type != null ? type.getId() : null;
    }
}
