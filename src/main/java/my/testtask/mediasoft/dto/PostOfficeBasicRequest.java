package my.testtask.mediasoft.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.testtask.mediasoft.mapper.Request;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Standard entity for post office registration")
public class PostOfficeBasicRequest implements Request {

    @Schema(description = "Post office code")
    private String index;
    @Schema(description = "Post office address")
    private String address;

}
