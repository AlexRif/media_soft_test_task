package my.testtask.mediasoft.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.testtask.mediasoft.mapper.Response;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "List element with information about the movement of the mailing")
public class MailingMovementLogBrowseResponse implements Response {

    @Schema(description = "Mailing id")
    private UUID mailingId;
    @Schema(description = "Post office id")
    private UUID postOfficeId;
    @Schema(description = "Date of movement registration")
    private Date createAt;
    @Schema(description = "type of movement (indicate in capital letters)", example = "ARRIVAL")
    private String type;

}
