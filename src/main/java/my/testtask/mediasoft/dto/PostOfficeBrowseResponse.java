package my.testtask.mediasoft.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.testtask.mediasoft.mapper.Response;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Post office information for listing")
public class PostOfficeBrowseResponse implements Response {

    private UUID id;
    @Schema(description = "Post office code")
    private String index;
    @Schema(description = "Post office address")
    private String address;

}
