package my.testtask.mediasoft.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.testtask.mediasoft.entity.mailing.enums.MailingMovementType;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Entity to register movement")
public class MailingMovementLogRequest {

    @Schema(description = "type of movement (indicate in capital letters)", example = "LITTER")
    private String type;
    @Schema(description = "Mailing id")
    private UUID mailingId;
    @Schema(description = "Post office id")
    private UUID postOfficeId;

    public MailingMovementType getType() {
        return this.type != null ? MailingMovementType.fromId(type) : null;
    }

    public void setType(MailingMovementType type) {
        this.type = type != null ? type.getId() : null;
    }
}
