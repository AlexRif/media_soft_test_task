package my.testtask.mediasoft.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class BasicModelMapper {

    private ModelMapper modelMapper;

    private org.modelmapper.ModelMapper getMapper() {
        if (modelMapper == null) {
            modelMapper = new org.modelmapper.ModelMapper();
            modelMapper.getConfiguration().setSkipNullEnabled(true);
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
        }
        return modelMapper;
    }

    public <D> D map(Object source, Class<D> destinationType) {
        return getMapper().map(source, destinationType);
    }

    public void map(Object source, Object destination) {
        getMapper().map(source, destination);
    }
}
