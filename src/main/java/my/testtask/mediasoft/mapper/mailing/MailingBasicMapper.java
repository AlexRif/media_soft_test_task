package my.testtask.mediasoft.mapper.mailing;

import my.testtask.mediasoft.dto.MailingRequest;
import my.testtask.mediasoft.entity.mailing.Mailing;
import my.testtask.mediasoft.mapper.BasicModelMapper;
import my.testtask.mediasoft.mapper.RequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MailingBasicMapper implements RequestMapper<Mailing, MailingRequest> {

    @Autowired
    private BasicModelMapper modelMapper;

    @Override
    public Mailing toEntity(MailingRequest request, Mailing entity) {
        modelMapper.map(request, entity);
        return entity;
    }
}
