package my.testtask.mediasoft.mapper.mailing;


import my.testtask.mediasoft.dto.PostOfficeBasicRequest;
import my.testtask.mediasoft.entity.mailing.PostOffice;
import my.testtask.mediasoft.mapper.BasicModelMapper;
import my.testtask.mediasoft.mapper.RequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostOfficeBasicMapper implements RequestMapper<PostOffice, PostOfficeBasicRequest> {


    @Autowired
    private BasicModelMapper modelMapper;


    @Override
    public PostOffice toEntity(PostOfficeBasicRequest request, PostOffice entity) {
        modelMapper.map(request, entity);
        return entity;
    }
}
