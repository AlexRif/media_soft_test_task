package my.testtask.mediasoft.mapper.mailing;

import my.testtask.mediasoft.dto.MailingMovementLogBrowseResponse;
import my.testtask.mediasoft.entity.mailing.MailingMovementLog;
import my.testtask.mediasoft.mapper.BasicModelMapper;
import my.testtask.mediasoft.mapper.ResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MailingMovementLogBrowseMapper implements ResponseMapper<MailingMovementLog, MailingMovementLogBrowseResponse> {


    @Autowired
    private BasicModelMapper modelMapper;

    @Override
    public MailingMovementLogBrowseResponse toResponse(MailingMovementLog entity) {
        MailingMovementLogBrowseResponse response = modelMapper.map(entity, MailingMovementLogBrowseResponse.class);
        return response;
    }
}
