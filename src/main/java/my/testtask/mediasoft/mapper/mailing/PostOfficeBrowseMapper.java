package my.testtask.mediasoft.mapper.mailing;

import my.testtask.mediasoft.dto.PostOfficeBrowseResponse;
import my.testtask.mediasoft.entity.mailing.PostOffice;
import my.testtask.mediasoft.mapper.BasicModelMapper;
import my.testtask.mediasoft.mapper.ResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostOfficeBrowseMapper implements ResponseMapper<PostOffice, PostOfficeBrowseResponse> {

    @Autowired
    private BasicModelMapper basicModelMapper;

    @Override
    public PostOfficeBrowseResponse toResponse(PostOffice entity) {
        return basicModelMapper.map(entity,PostOfficeBrowseResponse.class);
    }
}
