package my.testtask.mediasoft.mapper;

import javax.validation.constraints.NotNull;

public interface ResponseMapper<E, R extends Response>{

    R toResponse(@NotNull E entity);
}