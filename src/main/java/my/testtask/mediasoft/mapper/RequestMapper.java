package my.testtask.mediasoft.mapper;

import javax.validation.constraints.NotNull;

public interface RequestMapper<E, R extends Request> {

    E toEntity(R request, @NotNull E entity);
}

