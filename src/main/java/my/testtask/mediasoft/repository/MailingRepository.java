package my.testtask.mediasoft.repository;

import my.testtask.mediasoft.entity.mailing.Mailing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MailingRepository extends JpaRepository<Mailing, UUID> {

}