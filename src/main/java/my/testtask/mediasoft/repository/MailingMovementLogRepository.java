package my.testtask.mediasoft.repository;

import my.testtask.mediasoft.entity.mailing.Mailing;
import my.testtask.mediasoft.entity.mailing.MailingMovementLog;
import my.testtask.mediasoft.entity.mailing.PostOffice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MailingMovementLogRepository extends JpaRepository<MailingMovementLog, UUID> {

    Optional<MailingMovementLog> findByTypeAndMailing(String type, Mailing mailing);

    List<MailingMovementLog> findAllByMailingOrderByCreateAt(Mailing mailing);

    Optional<MailingMovementLog> findByTypeAndMailingAndPostOffice(String type, Mailing mailing, PostOffice postOffice);
}