package my.testtask.mediasoft.repository;

import my.testtask.mediasoft.entity.mailing.PostOffice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PostOfficeRepository extends JpaRepository<PostOffice, UUID> {
}