package my.testtask.mediasoft.exception;

public class ProcessException extends RuntimeException{

    public ProcessException(String message, Object ...params) {
        super(String.format(message, params));
    }
}
