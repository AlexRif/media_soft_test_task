package my.testtask.mediasoft.mailing;

import my.testtask.mediasoft.dto.MailingMovementLogRequest;
import my.testtask.mediasoft.entity.mailing.Mailing;
import my.testtask.mediasoft.entity.mailing.MailingMovementLog;
import my.testtask.mediasoft.entity.mailing.PostOffice;
import my.testtask.mediasoft.entity.mailing.enums.MailingMovementType;
import my.testtask.mediasoft.entity.mailing.enums.MailingType;
import my.testtask.mediasoft.exception.ProcessException;
import my.testtask.mediasoft.repository.MailingMovementLogRepository;
import my.testtask.mediasoft.repository.MailingRepository;
import my.testtask.mediasoft.repository.PostOfficeRepository;
import my.testtask.mediasoft.service.mailing.MailingMovementLogService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MailingMovementLogTest {

    @Autowired
    private MailingMovementLogService movementLogService;
    @Autowired
    private MailingRepository mailingRepository;
    @Autowired
    private MailingMovementLogRepository movementLogRepository;
    @Autowired
    private PostOfficeRepository postOfficeRepository;

    @BeforeEach
    @AfterEach
    void cleanDb() {
        this.movementLogRepository.deleteAll();
        this.mailingRepository.deleteAll();
        this.postOfficeRepository.deleteAll();
    }

    private Mailing mailing;
    private PostOffice postOffice;
    private MailingMovementLog movementLog;


    @Test
    void registryTest() {
        initDb();
        MailingMovementLogRequest movementLogRequest = new MailingMovementLogRequest();
        Assertions.assertThrows(ProcessException.class, () -> movementLogService.registry(movementLogRequest));
        movementLogRequest.setMailingId(mailing.getId());
        Assertions.assertThrows(ProcessException.class, () -> movementLogService.registry(movementLogRequest));
        movementLogRequest.setPostOfficeId(postOffice.getId());
        movementLogRequest.setType(MailingMovementType.ARRIVAL);
        MailingMovementLog resultLog = movementLogService.registry(movementLogRequest);
        Assertions.assertNotNull(resultLog);
        Assertions.assertNotNull(resultLog.getCreateAt());
        Assertions.assertEquals(movementLogRequest.getType(), resultLog.getType());
        Assertions.assertEquals(movementLogRequest.getMailingId(), resultLog.getMailing().getId());
        Assertions.assertEquals(movementLogRequest.getPostOfficeId(), resultLog.getPostOffice().getId());
        MailingMovementLog secondResult = movementLogService.registry(movementLogRequest);
        Assertions.assertEquals(resultLog.getId(), secondResult.getId());
    }


    @Test
    void registryDepartureTest() {
        initDb();
        MailingMovementLogRequest movementLogRequest = new MailingMovementLogRequest();
        Assertions.assertThrows(ProcessException.class, () -> movementLogService.registry(movementLogRequest));
        movementLogRequest.setMailingId(mailing.getId());
        Assertions.assertThrows(ProcessException.class, () -> movementLogService.registry(movementLogRequest));
        movementLogRequest.setPostOfficeId(postOffice.getId());
        movementLogRequest.setType(MailingMovementType.DEPARTURE);
        Assertions.assertThrows(ProcessException.class, () -> movementLogService.registry(movementLogRequest));

        movementLogRequest.setType(MailingMovementType.ARRIVAL);
        MailingMovementLog resultLog = movementLogService.registry(movementLogRequest);
        Assertions.assertNotNull(resultLog);
        Assertions.assertNotNull(resultLog.getCreateAt());
        Assertions.assertEquals(movementLogRequest.getType(), resultLog.getType());
        Assertions.assertEquals(movementLogRequest.getMailingId(), resultLog.getMailing().getId());
        Assertions.assertEquals(movementLogRequest.getPostOfficeId(), resultLog.getPostOffice().getId());
        MailingMovementLog secondResult = movementLogService.registry(movementLogRequest);
        Assertions.assertEquals(resultLog.getId(), secondResult.getId());
    }

    private void initDb() {
        mailing = new Mailing();
        mailing.setAddresseeIndex("443066");
        mailing.setFullName("Тестов Тест Тестович");
        mailing.setAddresseeAddress("г. Тест, ул. Тестовая, д.1");
        mailing.setType(MailingType.LITTER);
        mailing = mailingRepository.save(mailing);
        movementLog = new MailingMovementLog(MailingMovementType.REGISTRATION, mailing, null);
        movementLog = movementLogRepository.save(movementLog);
        postOffice = new PostOffice();
        postOffice.setAddress("г. Тестовый, ул. Тестовая, д. 2");
        postOffice.setIndex("443066");
        postOffice = postOfficeRepository.save(postOffice);
    }

}
