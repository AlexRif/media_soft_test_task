package my.testtask.mediasoft.mailing;


import my.testtask.mediasoft.dto.MailingRequest;
import my.testtask.mediasoft.entity.mailing.enums.MailingType;
import my.testtask.mediasoft.exception.ProcessException;
import my.testtask.mediasoft.repository.MailingMovementLogRepository;
import my.testtask.mediasoft.repository.MailingRepository;
import my.testtask.mediasoft.service.mailing.MailingService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MailingServiceTest {

    @Autowired
    private MailingService mailingService;
    @Autowired
    private MailingRepository mailingRepository;
    @Autowired
    private MailingMovementLogRepository movementLogRepository;

    @BeforeEach
    @AfterEach
    void cleanDb() {
        this.movementLogRepository.deleteAll();
        this.mailingRepository.deleteAll();
    }

    @Test
    void registryTest() {
        MailingRequest mailingRequest = new MailingRequest();
        Assertions.assertThrows(ProcessException.class, () -> mailingService.registry(null));
        Assertions.assertThrows(ProcessException.class, () -> mailingService.registry(mailingRequest));
        mailingRequest.setAddresseeIndex("443066");
        Assertions.assertThrows(ProcessException.class, () -> mailingService.registry(mailingRequest));
        mailingRequest.setFullName("Тестов Тест Тестович");
        Assertions.assertThrows(ProcessException.class, () -> mailingService.registry(mailingRequest));
        mailingRequest.setAddresseeAddress("г. Тест, ул. Тестовая, д.1");
        Assertions.assertThrows(ProcessException.class, () -> mailingService.registry(mailingRequest));
        mailingRequest.setType(MailingType.LITTER);
        Assertions.assertNotNull(mailingService.registry(mailingRequest));
        Assertions.assertEquals(1,mailingRepository.findAll().size());
        Assertions.assertEquals(1,movementLogRepository.findAll().size());
    }
}
